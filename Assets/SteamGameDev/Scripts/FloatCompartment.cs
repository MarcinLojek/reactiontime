﻿using UnityEngine;

namespace SteamGameDev
{
    public class FloatCompartment
    {
        private float _min;
        private float _max;
        
        public FloatCompartment(float min, float max)
        {
            _min = Mathf.Min(min, max);
            _max = Mathf.Max(min, max);
            InitialMin = _min;
            InitialMax = max;
        }
        
        public float InitialMin { get; private set;}
        public float InitialMax { get; private set; }

        public float Max
        {
            get { return _max; }
            set { _max = SetValue(value); }
        }

        public float Min
        {
            get { return _min; }
            set { _min = SetValue(value); }
        }

        private float SetValue(float value)
        {
            var maxLimit = Mathf.Min(InitialMax, _max);
            var minLimit = Mathf.Max(InitialMin, _min);

            return Mathf.Clamp(value, minLimit, maxLimit);
        }
    }
}