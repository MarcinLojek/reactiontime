﻿using thelab.mvc;

namespace SteamGameDev.UI
{
    public class NotificationSender : NotificationView<BaseApplication>
    {
        public virtual void Send()
        {
            Notify(notification);
        }
    }
}