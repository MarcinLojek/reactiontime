﻿using UnityEngine;
using UnityEngine.UI;

namespace SteamGameDev.UI
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(Image))]
    public class ImageSwaper : MonoBehaviour
    {
        public Sprite FirstImage;
        public Sprite SecondImage;

        private Image _thisObjectImage;

        private void Awake()
        {
            _thisObjectImage = GetComponent<Image>();
            SetSprite(FirstImage);
        }

        public void Swap(bool isSwaped)
        {
            SetSprite(isSwaped ? SecondImage : FirstImage);
        }

        private void SetSprite(Sprite sprite)
        {
            _thisObjectImage.sprite = sprite;
        }
    }
}

