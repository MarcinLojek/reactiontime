﻿using UnityEngine;

namespace SteamGameDev.UI
{
    public class ViewChangeNotificationSender : NotificationSender
    {
        public override void Send()
        {
            Notify(notification, GameObject.FindGameObjectWithTag("Interface").transform);
        }
    }
}