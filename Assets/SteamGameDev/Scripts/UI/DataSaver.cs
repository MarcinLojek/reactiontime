﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace SteamGameDev.UI
{
    public class DataSaver : MonoBehaviour
    {
        public InputField InputField;
        
        public void SaveInt(string name)
        {
            PlayerPrefs.SetInt(name, Convert.ToInt32(InputField.text));
            PlayerPrefs.Save();
        }
        
        public void SaveString(string name)
        {
            PlayerPrefs.SetString(name, InputField.text);
            PlayerPrefs.Save();
        }
        
        public void SaveFloat(string name)
        {
            PlayerPrefs.SetFloat(name, Convert.ToSingle(InputField.text));
            PlayerPrefs.Save();
        }
        
        public void SaveInt(Name name)
        {
            SaveInt(name.name);
        }
        
        public void SaveString(Name name)
        {
            SaveString(name.name);
        }
        
        public void SaveFloat(Name name)
        {
            SaveFloat(name.name);
        }
    }
}