﻿using UnityEngine;

namespace SteamGameDev
{
    public class Destroyer : MonoBehaviour {
        public void Destroy()
        {
            Destroy(gameObject);
        }

        public void Destroy(float time)
        {
            Destroy(gameObject, time);
        }
    }
}

