﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace SteamGameDev
{
    public class ActiveStateToggler : MonoBehaviour
    {
        public void ToggleActive()
        {
            gameObject.SetActive(!gameObject.activeSelf);
        }
    }
}