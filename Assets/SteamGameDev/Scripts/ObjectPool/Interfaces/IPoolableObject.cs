﻿using UnityEngine;

namespace SteamGameDev.ObjectPool
{
    public interface IPoolableObject
    {
        void Initialize(params object[] parameters);
        void Clean();

        bool ActiveInHierarchy { get; }

        IPoolableObject Clone();
    }

    public interface IPoolable : IPoolableObject
    {
        Transform ParenTransformOfClone { get; set; }
    }
}