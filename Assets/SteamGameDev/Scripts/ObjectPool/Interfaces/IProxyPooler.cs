﻿namespace SteamGameDev.ObjectPool
{
    public interface IProxyPooler
    {
        bool Aquire(out IPoolableObject instance);
        bool Release(IPoolableObject instance);
        int ActiveCount { get; }
    }
}