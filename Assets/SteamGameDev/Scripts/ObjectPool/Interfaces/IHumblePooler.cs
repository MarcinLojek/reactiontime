﻿namespace SteamGameDev.ObjectPool
{
    public interface IHumblePooler : IProxyPooler
    {
        void Instantiate();
    }
}