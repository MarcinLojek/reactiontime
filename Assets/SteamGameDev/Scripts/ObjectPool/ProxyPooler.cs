﻿namespace SteamGameDev.ObjectPool
{
    public class ProxyPooler : IProxyPooler
    {
        private IHumblePooler HumblePooler { get; set; }
        public bool WillGrow { get; set; }

        public ProxyPooler(IHumblePooler humblePooler, int poolSize)
        {
            HumblePooler = humblePooler;
            WillGrow = false;
            for (var index = 0; index < poolSize; index++)
            {
                humblePooler.Instantiate();
            }
        }

        public bool Aquire(out IPoolableObject instance)
        {
            var succeed = HumblePooler.Aquire(out instance);
            if (succeed) return true;

            if (!WillGrow) return false;

            HumblePooler.Instantiate();
            return HumblePooler.Aquire(out instance);
        }

        public bool Release(IPoolableObject instance)
        {
            return HumblePooler.Release(instance);
        }

        public int ActiveCount
        {
            get { return HumblePooler.ActiveCount; }
        }
    }
}