﻿using System.Collections.Generic;
using System.Linq;

namespace SteamGameDev.ObjectPool
{
    public class HumblePooler : IHumblePooler
    {
        private readonly ICollection<IPoolableObject> _unusedObjects;
        private readonly ICollection<IPoolableObject> _usedObjects;
        private IPoolableObject PoolableObject { get; set; }

        public HumblePooler(IPoolableObject poolableObject)
        {
            PoolableObject = poolableObject;
            _unusedObjects = new HashSet<IPoolableObject>();
            _usedObjects = new HashSet<IPoolableObject>();
        }

        public void Instantiate()
        {
            _unusedObjects.Add(PoolableObject.Clone());
        }

        public bool Aquire(out IPoolableObject instance)
        {
            instance = _unusedObjects.FirstOrDefault();
            if (instance == null) return false;

            _unusedObjects.Remove(instance);
            _usedObjects.Add(instance);
            return true;
        }

        public bool Release(IPoolableObject instance)
        {
            if (!_usedObjects.Remove(instance)) return false;
            _unusedObjects.Add(instance);
            instance.Clean();
            return true;
        }

        public int ActiveCount
        {
            get
            {
                return _unusedObjects.Count(x => x.ActiveInHierarchy) + _usedObjects.Count(x => x.ActiveInHierarchy);
            }
        }

        public int UnusedCount
        {
            get { return _unusedObjects.Count; }
        }

        public int UsedCount
        {
            get { return _usedObjects.Count; }
        }
    }
}