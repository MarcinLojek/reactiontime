﻿using UnityEngine;

namespace SteamGameDev.ObjectPool
{
    public class Pool : MonoBehaviour, IProxyPooler
    {
        private IHumblePooler _humblePooler;
        private IProxyPooler _proxyPooler;
        public GameObject Prefab;
        [SerializeField, Range(0, 1000)] private int _poolSize;
        [SerializeField] private bool _willGrow;

        private void Awake()
        {
            var poolableObject = Prefab.GetComponent<IPoolable>();
            poolableObject.ParenTransformOfClone = transform;
            _humblePooler = new HumblePooler(poolableObject);
            _proxyPooler = new ProxyPooler(_humblePooler, _poolSize) {WillGrow = _willGrow};
        }

        public bool Aquire(out IPoolableObject instance)
        {
            return _proxyPooler.Aquire(out instance);
        }

        public bool Release(IPoolableObject instance)
        {
            return _proxyPooler.Release(instance);
        }

        public int ActiveCount { get { return _proxyPooler.ActiveCount; } }
    }
}