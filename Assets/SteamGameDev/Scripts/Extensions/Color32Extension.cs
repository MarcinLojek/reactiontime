﻿using UnityEngine;

namespace SteamGameDev.Extensions
{
    public static class Color32Extension
    {
        public static string ToHex(this Color32 color)
        {
            return "#" + color.r.ToString("x2") + color.g.ToString("x2") + color.b.ToString("x2") + color.a.ToString("x2");
        }
    }
}