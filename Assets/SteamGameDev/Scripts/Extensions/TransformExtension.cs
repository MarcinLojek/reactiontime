﻿using UnityEngine;
using Object = UnityEngine.Object;

namespace SteamGameDev.Extensions
{
    public static class TransformExtension
    {
        public static void DestroyChildrens(this Transform transform)
        {
            foreach (Transform tran in transform)
            {
                Object.DestroyImmediate(tran.gameObject);
            }
        }

        public static bool HasChildren(this Transform transform)
        {
            return transform.childCount != 0;
        }
    }
}