using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace SteamGameDev.DependencyInjection
{
    [AttributeUsage(AttributeTargets.Field)]
    public sealed class InjectAttribute : Attribute
    {
    }

    public static class DependencyInjector
    {
        public static readonly Dictionary<Type, object> Components = new Dictionary<Type, object>();
        private static readonly object Placeholder = new object();

        private static readonly Dictionary<Type, FieldInfo[]> CachedFields = new Dictionary<Type, FieldInfo[]>();

        private const BindingFlags FieldFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance |
                                                BindingFlags.DeclaredOnly;

        public static void InjectObject(object target)
        {
            var fields = GetFields(target.GetType());
            foreach (var t in fields)
                t.SetValue(target, Resolve(t.FieldType));
        }

        private static IEnumerable<FieldInfo> GetFields(Type type)
        {
            FieldInfo[] fields;
            if (CachedFields.TryGetValue(type, out fields)) return fields;
            fields = GetInjectFields(type);
            CachedFields.Add(type, fields);
            return fields;
        }

        public static void Inject(this MonoBehaviour target)
        {
            InjectObject(target);
        }

        private static readonly List<FieldInfo> InjectFieldsBuffer = new List<FieldInfo>();

        private static FieldInfo[] GetInjectFields(Type type)
        {
            while (type != null)
            {
                var typeFields = type.GetFields(FieldFlags);
                foreach (var t in typeFields)
                {
                    if (HasInjectAttribute(t))
                        InjectFieldsBuffer.Add(t);
                }
                type = type.BaseType;
            }

            var resultFields = InjectFieldsBuffer.ToArray();
            InjectFieldsBuffer.Clear();
            return resultFields;
        }

        private static bool HasInjectAttribute(MemberInfo member)
        {
            return member.GetCustomAttributes(typeof(InjectAttribute), true).Any();
        }

        public static T Resolve<T>()
        {
            return (T) Resolve(typeof(T));
        }

        private static object Resolve(Type type)
        {
            object component;

            if (Components.TryGetValue(type, out component))
            {
                if (Placeholder == component)
                    throw new Exception("Cyclic dependency detected in " + type);
            }
            else
            {
                Components[type] = Placeholder;
                component = Components[type] = CreateComponent(type);
            }

            return component;
        }

        public static void ReplaceComponent<T>(T newComponent)
        {
            Components[typeof(T)] = newComponent;
            foreach (var c in Components.Values)
            {
                foreach (var f in GetFields(c.GetType()))
                {
                    if (f.FieldType == typeof(T)) f.SetValue(c, newComponent);
                }
            }
        }

        public static void ClearCache()
        {
            foreach (var componentPair in Components)
            {
                ((IDisposable) componentPair.Value).Dispose();
            }
            CachedFields.Clear();
            Components.Clear();
            GC.Collect();
        }

        private static object CreateComponent(Type type)
        {
            try
            {
                return Activator.CreateInstance(type);
            }
            catch (TargetInvocationException e)
            {
                if (e.InnerException != null) throw e.InnerException;
                throw;
            }
        }
    }
}