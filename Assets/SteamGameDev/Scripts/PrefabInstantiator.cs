﻿using UnityEngine;

namespace SteamGameDev
{
    public class PrefabInstantiator : MonoBehaviour
    {
        [SerializeField] private GameObject _prefab;
        [SerializeField] private GameObject _rootOfPrefab;
        
        public void InstantiatePrefab()
        {
            var instance = Instantiate(_prefab);
            instance.name = _prefab.name;
            instance.transform.SetParent(_rootOfPrefab.transform.parent, false);
        }
    }
}