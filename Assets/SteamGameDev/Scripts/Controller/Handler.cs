﻿using System.Collections.Generic;
using SteamGameDev.Commands;
using thelab.mvc;
using UnityEngine;

namespace SteamGameDev.UI
{
    public class Handler
    {
        private readonly ICommand _command;

        private Handler(ICommand command)
        {
            _command = command;
        }
        
        public void Execute<T>(T app, string pEvent, Object pTarget, params object[] pData) where T : BaseApplication
        {
            
            if (_command.Tag != pEvent)
            {
                if (Successor != null)
                    Successor.Execute(app, pEvent, pTarget, pData);
            }
            else
            {
                _command.Execute(app, pEvent, pTarget, pData);
            }
        }
        
        public Handler Successor { get; private set; }

        public static Handler Configure(IEnumerator<ICommand> enumerator)
        {
            enumerator.MoveNext();
            var first = Create(enumerator.Current);
            var previous = first;
            while (enumerator.MoveNext())
            {
                previous.Successor = Create(enumerator.Current);
                previous = previous.Successor;
            }

            return first;
        }

        private static Handler Create(ICommand command)
        {
            return new Handler(command);
        }
    }
}