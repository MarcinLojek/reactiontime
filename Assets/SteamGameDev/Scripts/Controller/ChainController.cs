﻿using System.Collections.Generic;
using System.Linq;
using SteamGameDev.Commands;
using thelab.mvc;
using UnityEngine;

namespace SteamGameDev.UI
{
    public class ChainController : Controller<BaseApplication>
    {
        [HideInInspector] public List<ControllerCommand> Commands;
        private Handler _handler;

        private void Awake()
        {
            _handler = Handler.Configure(Commands.Cast<ICommand>().GetEnumerator());
        }

        public override void OnNotification(string pEvent, Object pTarget, params object[] pData)
        {
            _handler.Execute(app, pEvent, pTarget, pData);
        }
    }
}