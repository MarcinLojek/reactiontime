﻿using System;
using thelab.mvc;
using UnityEngine;
using Object = UnityEngine.Object;

namespace SteamGameDev.Commands
{
    [CreateAssetMenu(fileName = "Command", menuName = "Application/Commands/Command"), Serializable]
    public class ControllerCommand : ScriptableObject, ICommand
    {
        [SerializeField] private string _tag;

        public virtual void Execute<T>(T app, string pEvent, Object pTarget, params object[] pData) where T : BaseApplication
        {
            throw new NotImplementedException();
        }

        public object Clone()
        {
            throw new NotImplementedException();
        }

        public string Tag
        {
            get { return _tag; }
        }
    }
}