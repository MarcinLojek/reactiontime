﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace SteamGameDev.Commands
{
    [CreateAssetMenu(fileName = "MultiCommand", menuName = "Application/Commands/MultiCommand"), Serializable]
    public class MultiCommand : ControllerCommand
    {
        [HideInInspector] public List<ControllerCommand> Commands;

        public override void Execute<T>(T app, string pEvent, Object pTarget, params object[] pData)
        {
            foreach (var command in Commands)
            {
                command.Execute(app, pEvent, pTarget, pData);
            }
        }
    }
}