﻿using System;
using thelab.mvc;
using Object = UnityEngine.Object;

namespace SteamGameDev.Commands
{
    public interface ICommand : ICloneable
    {
        string Tag { get;}
        void Execute<T>(T app, string pEvent, Object pTarget, params object[] pData) where T : BaseApplication;
    }
}