﻿using System;
using SteamGameDev.Extensions;
using UnityEngine;
using Object = UnityEngine.Object;

namespace SteamGameDev.Commands
{
    [CreateAssetMenu(fileName = "ChangeViewCommand", menuName = "Application/Commands/ChangeView"), Serializable]
    public class ChangeViewCommand : ControllerCommand
    {
        [SerializeField] private GameObject _view;
        
        public override void Execute<T>(T app, string pEvent, Object pTarget, params object[] pData)
        {
            var transform = pData[0] as Transform;

            transform.DestroyChildrens();
            Instantiate(_view, transform);
        }
    }
}