﻿namespace SteamGameDev.Commands
{
    public class CommandFactory
    {
        private CommandFactory()
        {
            
        }

        public ICommand Command { get; set; }

        public ICommand NewInstance
        {
            get { return Command.Clone() as ICommand; }
        }

        public static CommandFactory Factory()
        {
            return new CommandFactory();
        }
    }
}