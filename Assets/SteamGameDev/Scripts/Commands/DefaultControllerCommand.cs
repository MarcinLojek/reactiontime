﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace SteamGameDev.Commands
{
    [CreateAssetMenu(fileName = "DefaultCommand", menuName = "Application/Commands/DefaultCommand"), Serializable]
    public class DefaultControllerCommand : ControllerCommand
    {
        public override void Execute<T>(T app, string pEvent, Object pTarget, params object[] pData)
        {
            Debug.Log("Default Notification Service. UiApplication.UiController");
        }
    }
}