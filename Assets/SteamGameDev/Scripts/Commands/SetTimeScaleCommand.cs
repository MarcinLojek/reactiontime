﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace SteamGameDev.Commands
{
    [CreateAssetMenu(fileName = "SetTimaScaleCommand", menuName = "Application/Commands/SetTimeScale"), Serializable]
    public class SetTimeScaleCommand : ControllerCommand
    {
        public float TimeScale;
        public override void Execute<T>(T app, string pEvent, Object pTarget, params object[] pData)
        {
            Time.timeScale = TimeScale;
        }
    }
}