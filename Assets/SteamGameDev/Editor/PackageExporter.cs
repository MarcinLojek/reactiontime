﻿using System.Linq;
using UnityEditor;
using UnityEngine;

namespace SteamGameDev { 
    [CreateAssetMenu(fileName = "Exporter", menuName = "PackageExporter/Exporter")]
    public class PackageExporter : ScriptableObject
    {
        public string PackageName;
        public DefaultAsset[] Directories;
        public Object[] OtherFiles;

        public void Export()
        {
            var path = EditorUtility.OpenFolderPanel("Export location", "", "");
            
            var assetsDirectoriesPaths = GetAssetsPaths(Directories);
            var assetsOtherFilesPaths = GetAssetsPaths(OtherFiles);

            var allPaths = assetsDirectoriesPaths.Concat(assetsOtherFilesPaths).ToArray();

            ExportPackage(allPaths, path);
        }

        public string[] GetAssetsPaths(Object[] assets)
        {
            var length = assets.Length;
            var assetsPaths = new string[length];

            for (var i = 0; i < length; i++)
            {
                assetsPaths[i] = AssetDatabase.GetAssetPath(assets[i]);
            }

            return assetsPaths;
        }

        private void ExportPackage(string[] assetsFoldersPaths, string path)
        {
            AssetDatabase.ExportPackage(assetsFoldersPaths, path + "/" + PackageName + ".unitypackage",
                ExportPackageOptions.Interactive | ExportPackageOptions.Recurse);
        }
    }
}
