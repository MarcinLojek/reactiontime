﻿using Object = UnityEngine.Object;
using UnityEditor;

namespace SteamGameDev.Inspectors
{
    public abstract class Inspector<T> : Editor where T : Object
    {
        protected T Target;

        private void OnEnable()
        {
            Target = target as T;
            OnEnableHook();
        }
        
        protected virtual void OnEnableHook(){}
    }
}
