﻿using UnityEditor;
using UnityEngine;

namespace SteamGameDev.Inspectors
{
	[CustomEditor(typeof(PackageExporter)), CanEditMultipleObjects]
	public class PackageExporterInspector : Inspector<PackageExporter>
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			if (GUILayout.Button("Export"))
			{
				Target.Export();
			}
		}
	}
}