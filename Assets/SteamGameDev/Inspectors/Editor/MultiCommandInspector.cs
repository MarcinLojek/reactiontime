﻿using UnityEngine;
using SteamGameDev.Commands;
using UnityEditor;
using UnityEditorInternal;

namespace SteamGameDev.Inspectors
{
    [CustomEditor(typeof(MultiCommand)), CanEditMultipleObjects]
    public class MultiCommandInspector : Inspector<MultiCommand>
    {
        private ReorderableList _reorderableList;

        private void OnEnable()
        {
            _reorderableList = new ReorderableList(serializedObject, serializedObject.FindProperty("Commands"), true,
                true, true, true);
            _reorderableList.drawHeaderCallback += DrawHeader;
            _reorderableList.drawElementCallback += DrawElement;
        }

        private void OnDisable()
        {
            _reorderableList.drawHeaderCallback -= DrawHeader;
            _reorderableList.drawElementCallback -= DrawElement;
        }

        private static void DrawHeader(Rect rect)
        {
            GUI.Label(rect, "Command");
        }

        private void DrawElement(Rect rect, int index, bool active, bool focused)
        {
            EditorGUI.BeginChangeCheck();
            EditorGUI.PropertyField(rect, serializedObject.FindProperty("Commands").GetArrayElementAtIndex(index),
                GUIContent.none);

            if (!EditorGUI.EndChangeCheck()) return;
            EditorUtility.SetDirty(target);
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            _reorderableList.DoLayoutList();
            serializedObject.ApplyModifiedProperties();
        }
    }
}