﻿using UnityEngine;
using UnityEngine.UI;

namespace ReactionTest
{
    [RequireComponent(typeof(RectTransform))]
    public class ChartFiller : MonoBehaviour
    {
        [SerializeField] private RectTransform _scoreDurationChart;
        [SerializeField] private RectTransform _scoreDurationXLabel;
        [SerializeField] private RectTransform _scoreDurationYLabel;
        [SerializeField] private RectTransform _scoreReactionTimeChart;
        [SerializeField] private RectTransform _scoreReactionTimeXLabel;
        [SerializeField] private RectTransform _scoreReactionTimeYLabel;
        [SerializeField] private RectTransform _pointPrefab;
        [SerializeField] private Text _textPrefab;
        private GameData _gameData;

        private void Awake()
        {
            var obj = GameObject.FindGameObjectWithTag("GameData");
            if(obj == null) return;
            _gameData = obj.GetComponent<GameData>();
            
            var axisScaler = new AxisScaler(true);
            var scores = _gameData.GetScores();
            var durations = _gameData.GetDurations();
            var reactionTimes = _gameData.GetReactionTimes();
            var scaledScores = axisScaler.Scale(scores);
            var scaledDurations = axisScaler.Scale(durations);
            var scaledReactionTimes = axisScaler.Scale(reactionTimes);

            for (var i = 0; i < scaledScores.Length; i++)
            {
                SetAnchorOfPoints(_scoreDurationChart.transform, scaledDurations[i], scaledScores[i]);
                SetAnchorOfPoints(_scoreReactionTimeChart.transform, scaledScores[i], scaledReactionTimes[i]);

                SetLabelInY(_scoreDurationYLabel, scores[i].ToString("0"), scaledScores[i]);
                SetLabelInX(_scoreReactionTimeXLabel, scores[i].ToString("0"), scaledScores[i]);
                
                SetLabelInX(_scoreDurationXLabel, durations[i].ToString("0.000"), scaledDurations[i]);

                SetLabelInY(_scoreReactionTimeYLabel, reactionTimes[i].ToString("0.000"), scaledReactionTimes[i]);
                
            }
        }

        private void SetAnchorOfPoints(Transform parent, float x, float y)
        {
            var point = Instantiate(_pointPrefab.gameObject, parent).GetComponent<RectTransform>();
            point.anchorMin = new Vector2(x, y);
            point.anchorMax = new Vector2(x, y);
        }

        private void SetLabelInX(Transform parent, string value, float point)
        {
            var textTransform = CreateTextPrefab(parent, value);
            textTransform.gameObject.GetComponent<ChartLabelCollider>().CollideInAxis = ChartLabelCollider.Axis.X;
            textTransform.anchorMin = new Vector2(point, 0);
            textTransform.anchorMax = new Vector2(point, 1);
            textTransform.offsetMin = new Vector2(textTransform.offsetMin.x, 0);
            textTransform.offsetMax = new Vector2(textTransform.offsetMax.x, 0);
        }

        private void SetLabelInY(Transform parent, string value, float point)
        {
            var textTransform = CreateTextPrefab(parent, value);
            textTransform.gameObject.GetComponent<ChartLabelCollider>().CollideInAxis = ChartLabelCollider.Axis.Y;
            textTransform.anchorMin = new Vector2(0, point);
            textTransform.anchorMax = new Vector2(1, point);
            textTransform.offsetMin = new Vector2(0, textTransform.offsetMin.y);
            textTransform.offsetMax = new Vector2(0, textTransform.offsetMax.y);
        }

        private RectTransform CreateTextPrefab(Transform parent, string value)
        {
            var text = Instantiate(_textPrefab.gameObject, parent).GetComponent<Text>();
            text.text = value;
            return text.GetComponent<RectTransform>();
        }
    }
}