﻿using System.Collections;
using System.Collections.Generic;
using ReactionTest;
using UnityEngine;

namespace ReactionTest
{
    public class GameData : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            Results = new List<Result> {Result.InitialResult()};
        }

        public List<Result> Results { get; private set; }

        public void SetResultes(IEnumerator<Result> enumerator)
        {
            Results = new List<Result>();
            while (enumerator.MoveNext())
            {
                Results.Add(enumerator.Current);
            }
        }

        public float[] GetScores()
        {
            var scores = new float[Results.Count];
            for (var i = 0; i < Results.Count; i++)
            {
                scores[i] = Results[i].Score;
            }

            return scores;
        }
        
        public float[] GetDurations()
        {
            var durations = new float[Results.Count];
            for (var i = 0; i < Results.Count; i++)
            {
                durations[i] = Results[i].Duration;
            }

            return durations;
        }
        
        public float[] GetReactionTimes()
        {
            var reactionTimes = new float[Results.Count];
            for (var i = 0; i < Results.Count; i++)
            {
                reactionTimes[i] = Results[i].ReactionTime;
            }

            return reactionTimes;
        }   
    }
}