﻿namespace ReactionTest
{
    public class Result
    {
        private Result(
            float reactionTime, 
            int score,
            float sumaricalReactionTime, 
            float shortestReactionTime,
            float averageReactionTime, 
            float rate, 
            float duration)
        {
            ReactionTime = reactionTime;
            Score = score;
            SumaricalReactionTime = sumaricalReactionTime;
            ShortestReactioinTime = shortestReactionTime;
            AverageReactionTime = averageReactionTime;
            Rate = rate;
            Duration = duration;
        }

        public static Result InitialResult()
        {
            return new Result(0, 0, 0, float.MaxValue, 0, 0, 0);
        }

        public Result Update(float reactionTime, float duration)
        {
            var score = Score + 1;
            var sumaricalReactionTime = SumaricalReactionTime + reactionTime;
            var shortest = reactionTime < ShortestReactioinTime ? reactionTime : ShortestReactioinTime;
            return new Result(
                reactionTime, 
                score,
                sumaricalReactionTime, 
                shortest,
                sumaricalReactionTime / score,
                duration / score, 
                duration);
        }

        public float ReactionTime { get; private set; }
        public int Score { get; private set; }
        public float SumaricalReactionTime { get; private set; }
        public float AverageReactionTime { get; private set; }
        public float ShortestReactioinTime { get; private set; }
        public float Rate { get; private set; }
        public float Duration { get; private set; }
    }
}