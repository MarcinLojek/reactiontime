﻿using System.Collections.Generic;
using thelab.mvc;
using UnityEngine;

namespace ReactionTest
{
    public class RtModel : Model<RtApplication>
    {
        public delegate void ScoreChanged(Result result);
        public event ScoreChanged OnScoreChanged;
        
        private List<Result> _results;
        
        private void Awake()
        {
            _results = new List<Result>();
            LastResult = Result.InitialResult();
        }

        public void Scored(float reactionTime)
        {
            LastResult = LastResult.Update(reactionTime, Time.timeSinceLevelLoad);
            _results.Add(LastResult);
            if (OnScoreChanged != null) OnScoreChanged.Invoke(LastResult);
        }

        public Result LastResult { get; private set; }

        public IEnumerator<Result> Results
        {
            get
            {
                return _results.GetEnumerator();
            }
        }
    }
}