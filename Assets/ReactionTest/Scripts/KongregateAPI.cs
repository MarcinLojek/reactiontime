﻿using System;
using UnityEngine;

public class KongregateAPI : MonoBehaviour
{
    private static string DEFAULT_USER_NAME = "Guest";
    private static bool KONGREGATE_LOADED;
    private static int USER_ID;
    private static string USER_NAME = DEFAULT_USER_NAME;
    private string GAME_AUTHENTICATION_TOKEN = "";

    private static KongregateAPI _KongregateAPI;

    public void Start()
    {
        if (_KongregateAPI == null)
        {
            _KongregateAPI = this;
        }
        else if (_KongregateAPI != this)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject.transform.parent.gameObject);
        gameObject.name = "KongregateAPI";

        Application.ExternalEval(
            @"if(typeof(kongregateUnitySupport) != 'undefined'){
        kongregateUnitySupport.initAPI('KongregateAPI', 'OnKongregateAPILoaded');
      };"
        );
    }

    public void OnKongregateAPILoaded(string userInfoString)
    {
        KONGREGATE_LOADED = true;
        OnKongregateUserInfo(userInfoString);
    }

    public void OnKongregateUserInfo(string userInfoString)
    {
        string[] userStats = userInfoString.Split('|');
        USER_ID = Convert.ToInt32(userStats[0]);
        USER_NAME = userStats[1];
        GAME_AUTHENTICATION_TOKEN = userStats[2];
        Debug.Log("Kongregate User Info: " + USER_NAME + ", userId: " + USER_ID);
    }

    public static bool APILoaded()
    {
        if (USER_NAME != DEFAULT_USER_NAME && KONGREGATE_LOADED)
        {
            return true;
        }
        return false;
    }

    public static void SubmitData(string dataName, int dataValue)
    {
        if (APILoaded())
        {
            Application.ExternalCall("kongregate.stats.submit", dataName, dataValue);
        }
    }
}
