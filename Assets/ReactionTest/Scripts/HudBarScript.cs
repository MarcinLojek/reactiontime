﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class HudBarScript : MonoBehaviour
{
    public float Phase;
    public float StartFilling;
    public float MaximumStretch;
    
    private Image _uiImage;

    private void Start()
    {
        _uiImage = GetComponent<Image>();
    }

    private void Update()
    {
        _uiImage.fillAmount = (StartFilling + (Mathf.Sin(Phase + Time.time) + 1f) / 2f) * MaximumStretch;
    }
}