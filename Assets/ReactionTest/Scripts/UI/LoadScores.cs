﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace ReactionTest
{
    public class LoadScores : MonoBehaviour
    {
        private Result _lastResult;
        private GameData _gameData;
        [SerializeField] private Text _durationText, _scoreText, _shortestText, _averageText;

        private void Awake()
        {
            var obj = GameObject.FindGameObjectWithTag("GameData");
            if(obj == null) return;
            _gameData = obj.GetComponent<GameData>();
        }

        private void Start()
        {
            if(_gameData == null) return;
            _lastResult = _gameData.Results.Last();
            
            _durationText.text = GetNumericFormatedText("Duration: ", 1000, _lastResult.Duration);
            _scoreText.text = "Score: " + _lastResult.Score;
            _shortestText.text = GetNumericFormatedText("Shortest: ", 10,  _lastResult.ShortestReactioinTime);
            _averageText.text = GetNumericFormatedText("Average: ", 10, _lastResult.AverageReactionTime);
        }
        
        private static string GetNumericFormatedText(string text, int formatingTreshold, double value)
        {
            if (value > formatingTreshold)
            {
                text += value;
            }
            else
            {
                text += value.ToString("0.000");
            }
            return text;
        }
    }
}