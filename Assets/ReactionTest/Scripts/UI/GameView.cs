﻿using thelab.mvc;
using UnityEngine;
using UnityEngine.UI;

namespace ReactionTest.UI
{
    public class GameView : View<RtApplication>
    {
        public Text DurationText;
        public Text AveragetText;
        public Text ShortestText;
        public Text ScoreText;
        
        private void Awake()
        {
            InvokeRepeating("UpdateDuration", 0, 0.1f);
        }

        private void OnEnable()
        {
            app.model.OnScoreChanged += OnScoreChanged;
            if(app.model.LastResult == null) return;
            OnScoreChanged(app.model.LastResult);
        }

        private void OnDisable()
        {
            app.model.OnScoreChanged -= OnScoreChanged;
        }

        private void OnScoreChanged(Result result)
        {
            AveragetText.text = "Average: " + result.AverageReactionTime.ToString("0.00");
            ShortestText.text = "Shortest: " + result.ShortestReactioinTime.ToString("0.00");
            ScoreText.text = "Score: " + result.Score.ToString("0");
        }

        private void UpdateDuration()
        {
            DurationText.text = "Duration: " + Time.timeSinceLevelLoad.ToString("0.0");
        }
    }
}