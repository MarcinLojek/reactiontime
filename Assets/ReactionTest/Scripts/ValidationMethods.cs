﻿using System;
using System.Linq;
using UnityEngine;

namespace SteamGameDev
{
	public class ValidationMethods
	{
		private static readonly InclusiveInclusiveComparment AllInclusiveComparment = new InclusiveInclusiveComparment();
		private static readonly InclusiveExclusiveComparment InclusiveExclusiveComparment = new InclusiveExclusiveComparment();

		#region AreEqual

		public static bool AreEqual(int firstValue, int secondValue)
		{
			return firstValue == secondValue;
		}

		public static bool AreEqual(float firstValue, float secondValue, float delta)
		{
			return Math.Abs(firstValue - secondValue) < delta;
		}

		#endregion

		public static void IsValueInRange(int value, int min, int max)
		{
			IsValueInRange(value, min, max, "", AllInclusiveComparment);
		}

		public static void IsValueInRange(float value, float min, float max)
		{
			IsValueInRange(value, min, max, "", AllInclusiveComparment);
		}

		public static void IsValueInRangeRightExclusive(int value, int min, int max)
		{
			IsValueInRange(value, min, max, "", InclusiveExclusiveComparment);
		}

		public static void IsValueInRangeRightExclusive(float value, float min, float max)
		{
			IsValueInRange(value, min, max, "", InclusiveExclusiveComparment);
		}

		public static void IsValueInRange(int value, int min, int max, string message)
		{
			IsValueInRange(value, min, max, message, AllInclusiveComparment);
		}

		public static void IsValueInRangeRightExclusive(int value, int min, int max, string message)
		{
			IsValueInRange(value, min, max, message, InclusiveExclusiveComparment);
		}

		public static void IsValueInRange(int value, int min, int max, string message, Comparment comparment)
		{
			var isInRange = comparment.IsInRange(value, min, max);

			IfIsNotInRangeThrowException(isInRange, value.ToString(), min.ToString(), max.ToString(), message);
		}

		public static void IsValueInRange(float value, float min, float max, string message, Comparment comparment)
		{
			var isInRange = comparment.IsInRange(value, min, max);

			IfIsNotInRangeThrowException(isInRange, value.ToString(), min.ToString(), max.ToString(), message);
		}

		private static void IfIsNotInRangeThrowException(bool isInRange, string value, string min, string max, string message)
		{
			var exceptionMessage = ValueInRangeBasicMessage(value, min, max, "<=", "<=") + message;
			Exception exception = new ArgumentOutOfRangeException("", exceptionMessage);
			Validate(!isInRange, exception);
		}

		private static string ValueInRangeBasicMessage(string value, string min, string max, string firstCompareMark, string secondCompareMark)
		{
			return "Given value: " + value + " is out of range: " + min + firstCompareMark + value + secondCompareMark + max + ". ";
		}

		#region IsNull

		public static void IsNull(object argument)
		{
			IsNull(argument, "");
		}

		public static void IsNull(object argument, string message)
		{
			if (ReferenceEquals(null, argument)) {
				throw new ArgumentNullException("", message);
			}
		}

		#endregion

		public static void Validate(bool expression, Exception exception)
		{
			if (expression) {
				throw exception;
			}
		}

		public static bool Compare <T>(T[] list1, T[] list2)
		{
			return(list1.Length == list2.Length) && !list1.Except(list2).Any();
		}

		public static float FitToPercentageRange(float value)
		{
			return Mathf.InverseLerp(0, 100, value);
		}
	}
}