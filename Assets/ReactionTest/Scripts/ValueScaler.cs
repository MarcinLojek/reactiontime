﻿using SteamGameDev;

namespace ReactionTest
{
    public class ValueScaler
    {
        private float Current { get; set; }
        public float StartingValue { get; private set; }
        public float MinimalValue { get; private set; }
        public float Scale { get; private set; }

        public ValueScaler(float startingValue, float minimalValue, float scale)
        {
            StartingValue = startingValue;
            MinimalValue = minimalValue;
            ValidationMethods.IsValueInRange(scale, 0, 1f);
            Scale = scale;
            Current = StartingValue;
        }

        public float Apply()
        {
            var newScale = Current * Scale;
            if (newScale < MinimalValue)
            {
                return MinimalValue;
            }
            Current = newScale;
            return newScale;
        }
    }
}