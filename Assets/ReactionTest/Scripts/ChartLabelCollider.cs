﻿using System;
using UnityEngine;

public class ChartLabelCollider : MonoBehaviour
{
    public enum Axis
    {
        X,
        Y
    }

    public Axis CollideInAxis = Axis.X;

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!other.gameObject.CompareTag("Chart Label")) return;

        var myTransform = gameObject.GetComponent<RectTransform>();
        var otherTransform = other.gameObject.GetComponent<RectTransform>();
        switch (CollideInAxis)
        {
            case Axis.X:
            {
                var mean = (myTransform.anchorMax.x + otherTransform.anchorMax.x) / 2;
                if (mean > 0.5f)
                {
                    if (myTransform.anchorMax.x > otherTransform.anchorMax.x)
                    {
                        otherTransform.gameObject.SetActive(false);
                    }
                    else
                    {
                        myTransform.gameObject.SetActive(false);
                    }  
                }
                else
                {
                    if (myTransform.anchorMax.x < otherTransform.anchorMax.x)
                    {
                        otherTransform.gameObject.SetActive(false);
                    }
                    else
                    {
                        myTransform.gameObject.SetActive(false);
                    }  
                }
                break;
            }
            case Axis.Y:
            {
                var mean = (myTransform.anchorMax.y + otherTransform.anchorMax.y) / 2;
                if (mean > 0.5f)
                {
                    if (myTransform.anchorMax.y > otherTransform.anchorMax.y)
                    {
                        otherTransform.gameObject.SetActive(false);
                    }
                    else
                    {
                        myTransform.gameObject.SetActive(false);
                    }  
                }
                else
                {
                    if (myTransform.anchorMax.y < otherTransform.anchorMax.y)
                    {
                        otherTransform.gameObject.SetActive(false);
                    }
                    else
                    {
                        myTransform.gameObject.SetActive(false);
                    }  
                }
                break;
            }
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}