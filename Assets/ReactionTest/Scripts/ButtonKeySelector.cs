﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonKeySelector : MonoBehaviour
{
	public KeyCode Key;

	private void Update()
	{
		if (Input.GetKey(Key))
		{
			EventSystem.current.SetSelectedGameObject(gameObject);
		}
	}
}