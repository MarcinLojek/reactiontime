﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ReactionTest
{
    public class AxisScaler
    {
        private bool _scaleFromZero;
        
        public AxisScaler(bool scaleFromZero = false)
        {
            _scaleFromZero = scaleFromZero;
        }
        
        public float[] Scale(float[] data)
        {
            var scales = new List<float>();
            switch (data.Length)
            {
                case 0:
                    return scales.ToArray();
                case 1:
                    scales.Add(0.5f);
                    return scales.ToArray();
                default:
                    var min = _scaleFromZero ? 0 : data.Min();
                    var max = data.Max();

                    scales.AddRange(data.Select(value => Mathf.InverseLerp(min, max, value)));
                    return scales.ToArray();
            }
        }
    }
}