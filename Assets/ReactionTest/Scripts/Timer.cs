﻿using UnityEngine;

namespace ReactionTest
{
    public class Timer
    {
        public delegate void TimerAction();

        public event TimerAction OnOver;
        public event TimerAction OnStop;
        public event TimerAction OnRestart;

        private bool _isOver;

        public Timer(float startingTime)
        {
            StartingTime = startingTime;
            _isOver = false;
        }

        public void Update(float time)
        {
            if (_isOver) return;
            var absTime = Mathf.Abs(time);
            if (ElapsedTime + absTime < StartingTime)
            {
                ElapsedTime += absTime;
            }
            else
            {
                ElapsedTime = StartingTime;
                _isOver = true;

                if (OnOver != null) OnOver.Invoke();
            }
        }

        public void Stop()
        {
            _isOver = true;
            if (OnStop != null) OnStop.Invoke();
        }


        public void Restart()
        {
            _isOver = false;
            ElapsedTime = 0;
            if (OnRestart != null) OnRestart.Invoke();
        }

        public void Restart(float startingTime)
        {
            StartingTime = startingTime;
            Restart();
        }

        public float StartingTime { get; private set; }

        public float ElapsedTime { get; private set; }

        public float RemainingTime
        {
            get { return StartingTime - ElapsedTime; }
        }

        public float NormalizedElapsedTime
        {
            get { return Mathf.InverseLerp(0, StartingTime, ElapsedTime); }
        }

        public float NormalizedRemainingTime
        {
            get { return Mathf.InverseLerp(0, StartingTime, RemainingTime); }
        }
    }
}