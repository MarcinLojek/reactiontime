﻿using SteamGameDev;
using UnityEngine;

namespace ReactionTest
{
    public class ScreenPositioner : IPositioner
    {
        private readonly float _minX, _maxX, _minY, _maxY;
        
        public ScreenPositioner(float minX, float maxX, float minY, float maxY)
        {
            ValidationMethods.IsValueInRange(minX, 0, maxX);
            ValidationMethods.IsValueInRange(maxX, minX, 1);
            ValidationMethods.IsValueInRange(minY, 0, maxY);
            ValidationMethods.IsValueInRange(maxY, minY, 1);
            _minX = minX;
            _maxX = maxX;
            _minY = minY;
            _maxY = maxY;
        }
        
        public Vector3 Position
        {
            get
            {
                var x = Random.Range(Screen.width * _minX, Screen.width * _maxX);
                var y = Random.Range(Screen.height * _minY, Screen.height * _maxY);
                var z = Mathf.Abs(Camera.main.transform.position.z);
                var vector = new Vector3(x,y,z);
                return Camera.main.ScreenToWorldPoint(vector); }
        }
    }

    public interface IPositioner
    {
        Vector3 Position { get; }
    }
}