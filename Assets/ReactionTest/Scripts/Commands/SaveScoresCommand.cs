﻿using System;
using ReactionTest;
using SteamGameDev.Commands;
using UnityEngine;
using Object = UnityEngine.Object;

namespace SteamGameDev.Commads
{
    [CreateAssetMenu(fileName = "SaveDataCommand", menuName = "Application/Commands/SaveData"), Serializable]
    public class SaveScoresCommand : ControllerCommand
    {
        [SerializeField] private GameObject _gameDataPrefab;
        private GameData _gameData;
        private RtApplication _app;

        private void Awake()
        {
            _gameData = _gameDataPrefab.GetComponent<GameData>();
            if (!_gameData) throw new Exception("GameDataPrefab hasn't GameData script.");
        }
        
        public override void Execute<T>(T app, string pEvent, Object pTarget, params object[] pData)
        {
            _app = app as RtApplication;
            var dataHolder = GameObject.FindGameObjectWithTag("GameData");
            if (!dataHolder)
            {
                dataHolder = Instantiate(_gameDataPrefab);
            }

            _gameData = dataHolder.GetComponent<GameData>();
            Debug.Assert(_app != null, "_app != null");
            if (!_app.model.Results.MoveNext())
            {
                _app.model.Scored(0);
            }
            _gameData.SetResultes(_app.model.Results);
        }
    }
}