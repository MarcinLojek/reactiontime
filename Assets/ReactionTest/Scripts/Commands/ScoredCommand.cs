﻿using System;
using ReactionTest;
using SteamGameDev.Commands;
using UnityEngine;
using Object = UnityEngine.Object;

namespace SteamGameDev.Commads
{
    [CreateAssetMenu(fileName = "ScoredCommand", menuName = "Application/Commands/Scored"), Serializable]
    public class ScoredCommand : ControllerCommand
    {
        private RtApplication _app;
        
        public override void Execute<T>(T app, string pEvent, Object pTarget, params object[] pData)
        {
            _app = app as RtApplication;
            Debug.Assert(_app != null, "_app != null");
            Debug.Assert(pData.Length == 1, "pData.Length is different form 1.");
            Debug.Assert(pData[0] is float, "pData[0] is not float.");
            _app.model.Scored((float) pData[0]);
            _app.view.Spawner.GetComponent<SpawnManager>().Scored();
        }
    }
}