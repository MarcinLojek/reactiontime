﻿using System;
using SteamGameDev.Commands;
using SteamGameDev.Extensions;
using SteamGameDev.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace SteamGameDev.Commads
{
	[CreateAssetMenu(fileName = "LoadSceneCommand", menuName = "Application/Commands/LoadScene"), Serializable]
	public class LoadSceneCommand : ControllerCommand
	{
		public int SceneIndex;
		private UiApplication _app; 
		public override void Execute<T>(T app, string pEvent, Object pTarget, params object[] pData)
		{
			_app = app as UiApplication;
			SceneManager.sceneLoaded += OnSceneLoaded;
			SceneManager.LoadScene(SceneIndex);
		}

		private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
		{
			if (!_app) return;

			_app.view.Interface.transform.DestroyChildrens();
			SceneManager.sceneLoaded -= OnSceneLoaded;
		}
	}
}


