﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
	public Text FinalDurationValueLabel, FinalReactionTimeValueLabel;
	public Text DurationValueLabel, ReactionTimeValueLabel;

	private UnityAction _renderResultListener;
	private UnityAction<float> _scoredListener;
	private UnityAction _invokeCancelListener;

	private float _sumaricalReactionTime;
	private int _count;

	private void Awake()
	{
		_renderResultListener = RenderResults;
		_scoredListener = Scored;
		_invokeCancelListener = CancelInvoke;
	}

	private void Start()
	{
		_count = 0;
		_sumaricalReactionTime = 0;
		InvokeRepeating("RenderDuration", 0, 0.1f);
	}

	private void OnEnable()
	{
//		EventManager.StartListening("RenderResults", _renderResultListener);
//		EventManager.StartListening("Scored", _scoredListener);
//		EventManager.StartListening("CancelInvoke", _invokeCancelListener);
	}

	private void OnDisable()
	{
//		EventManager.StopListening("RenderResults", _renderResultListener);
//		EventManager.StopListening("Scored", _scoredListener);
//		EventManager.StopListening("CancelInvoke", _invokeCancelListener);
	}

	private void RenderResults()
	{
		FinalDurationValueLabel.text = Time.timeSinceLevelLoad.ToString("0.0");
		KongregateAPI.SubmitData("DURATION", (int)Math.Floor(Time.time * 1000));
		if (float.IsNaN(ReactionTime))
		{
			KongregateAPI.SubmitData("REACTIONSPEED", 0);
			FinalReactionTimeValueLabel.text = "0";
		}
		else
		{
			KongregateAPI.SubmitData("REACTIONSPEED", (int)Math.Floor(ReactionTime * 1000));
			FinalReactionTimeValueLabel.text = ReactionTime.ToString("0.00");
		}
	}

	private void Scored(float reactionTime)
	{
		_count++;
		_sumaricalReactionTime += reactionTime;
		ReactionTimeValueLabel.text = ReactionTime.ToString("0.00");
	}

	private void RenderDuration()
	{
		DurationValueLabel.text = Time.timeSinceLevelLoad.ToString("0.0");
	}

	public float ReactionTime
	{
		get
		{
			return _sumaricalReactionTime / _count;
		}
	}
}
