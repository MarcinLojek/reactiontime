﻿namespace SteamGameDev
{
	public abstract class Comparment
	{
		public readonly string FirstCompareMark;
		public readonly string SecondCompareMark;

		protected Comparment(string firstCompareMark, string secondCompareMark)
		{
			FirstCompareMark = firstCompareMark;
			SecondCompareMark = secondCompareMark;
		}

		public abstract bool IsInRange(int value, int min, int max);

		public abstract bool IsInRange(float value, float min, float max);
	}
}

