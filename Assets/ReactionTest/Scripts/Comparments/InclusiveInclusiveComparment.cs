﻿namespace SteamGameDev
{
	public class InclusiveInclusiveComparment : Comparment
	{
		public InclusiveInclusiveComparment()
			: base("<=", "<=")
		{
		}

		public override bool IsInRange(int value, int min, int max)
		{
			return(min <= value && value <= max);
		}

		public override bool IsInRange(float value, float min, float max)
		{
			return (min <= value && value <= max);
		}
	}
}