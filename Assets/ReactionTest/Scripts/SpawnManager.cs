﻿using SteamGameDev.ObjectPool;
using thelab.mvc;
using UnityEngine;

namespace ReactionTest
{
    public class SpawnManager : View<RtApplication>
    {
        public float StartingSpawnTime,
            MinimalSpawnTime,
            SpawnScale,
            StartingDisapearTime,
            MinimalDisapearTime,
            DisapearScale;

        private Spawner _spawner;
        private ValueScaler _spawnTimeScaler;
        private ValueScaler _disapearTimeScaler;

        private void Awake()
        {
            _spawnTimeScaler = new ValueScaler(StartingSpawnTime, MinimalSpawnTime, SpawnScale);
            _disapearTimeScaler = new ValueScaler(StartingDisapearTime, MinimalDisapearTime, DisapearScale);
            IPositioner positioner = new ScreenPositioner(0.2f, 0.8f, 0.2f, 0.8f);
            _spawner = new Spawner(app.view.Pool.GetComponent<Pool>(), positioner, _spawnTimeScaler,
                _disapearTimeScaler);
        }

        private void Start()
        {
            _spawner.Spawn();
        }

        private void Update()
        {
            _spawner.Update(Time.deltaTime);
        }

        public void Scored()
        {
            _spawner.Scored();
        }
    }
}