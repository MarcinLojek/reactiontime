﻿using System;
using SteamGameDev.ObjectPool;
using thelab.mvc;
using UnityEngine;

namespace ReactionTest
{
    [Serializable]
    public class RtView : View<RtApplication>
    {
        public Canvas Interface;
        public GameObject GameView;
        public SpawnManager Spawner;
        public Pool Pool;
    }
}