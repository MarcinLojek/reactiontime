﻿using SteamGameDev.ObjectPool;
using thelab.mvc;
using UnityEngine;

namespace ReactionTest
{
    [RequireComponent(typeof(SpriteRenderer), typeof(AudioSource))]
    public class TargetView : View<RtApplication>, IPoolable
    {
        public AudioClip TargetHitAudio;
        public AudioClip TargetMissAudio;
        
        public Color EndingColor;
        public Vector3 EndingScale;

        private Timer _timer;
        private SpriteRenderer _spriteRenderer;
        private Color _startingColor;
        private Vector3 _startingScale;
        private AudioSource _audioSource;
        
        private void Awake()
        {
            if (_timer == null) _timer = new Timer(3);
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _audioSource = GetComponent<AudioSource>();
        }

        private void OnEnable()
        {
            _timer.OnOver += OnTimeOver;
            _startingColor = _spriteRenderer.color;
            _startingScale = transform.localScale;
        }

        private void OnDisable()
        {
            _timer.OnOver -= OnTimeOver;
            _spriteRenderer.color = _startingColor;
            transform.localScale = _startingScale;
        }

        private void Update()
        {
            _timer.Update(Time.deltaTime);
            _spriteRenderer.color = Color.Lerp(_startingColor, EndingColor, _timer.NormalizedElapsedTime);
            transform.localScale = Vector3.Lerp(_startingScale, EndingScale, _timer.NormalizedElapsedTime);
        }

        private void OnMouseDown()
        {
            AudioSource.PlayClipAtPoint(TargetHitAudio, Camera.main.transform.position);
            Notify("game.scored", _timer.ElapsedTime);
            GetComponentInParent<Pool>().Release(this);
        }

        private void OnTimeOver()
        {
            AudioSource.PlayClipAtPoint(TargetMissAudio, Camera.main.transform.position);
            Notify("game.end");
            GetComponentInParent<Pool>().Release(this);
        }

        public void Initialize(params object[] parameters)
        {
            var spawnTime = (float) parameters[0];
            if (_timer == null)
            {
                _timer = new Timer(spawnTime);
            }
            else
            {
                _timer.Restart(spawnTime);
            }

            gameObject.SetActive(true);
            gameObject.transform.position = (Vector3) parameters[1];
            gameObject.transform.rotation = Quaternion.identity;
        }

        public void Clean()
        {
            gameObject.SetActive(false);
        }

        public bool ActiveInHierarchy
        {
            get { return gameObject.activeInHierarchy; }
        }

        public IPoolableObject Clone()
        {
            var obj = Instantiate(gameObject, ParenTransformOfClone);
            return obj.GetComponent<TargetView>();
        }

        public Transform ParenTransformOfClone { get; set; }
    }
}