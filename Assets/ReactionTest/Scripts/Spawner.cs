﻿using SteamGameDev.ObjectPool;

namespace ReactionTest
{
    public class Spawner
    {
        private IProxyPooler Pooler { get; set; }
        private IPositioner Positioner { get; set; }
        private ValueScaler SpawnScaler { get; set; }
        private ValueScaler DisapearScaler { get; set; }
        
        private readonly Timer _timer;

        public Spawner(IProxyPooler pooler, IPositioner positioner, ValueScaler spawnScaler, ValueScaler disapearScaler)
        {
            Pooler = pooler;
            Positioner = positioner;
            SpawnScaler = spawnScaler;
            DisapearScaler = disapearScaler;
            _timer = new Timer(SpawnScaler.StartingValue);
            _timer.OnOver += Spawn;
        }

        public void Update(float time)
        {
            _timer.Update(time);
        }

        public void Scored()
        {
            if (Pooler.ActiveCount >= 2) return;
            Spawn();
        }

        public void Spawn()
        {
            IPoolableObject obj;

            if (!Pooler.Aquire(out obj)) return;
            obj.Initialize(DisapearScaler.Apply(), Positioner.Position);
            _timer.Restart(SpawnScaler.Apply());
        }
    }
}