﻿using System;
using NUnit.Framework;

namespace ReactionTest.Tests
{
    [TestFixture]
    public class ValueScalerTest
    {
        [Test]
        public void Should_Create_Instance()
        {
            var scaler = new ValueScaler(10, 1, 0.99f);
            Assert.NotNull(scaler);
            Assert.AreEqual(10, scaler.StartingValue);
            Assert.AreEqual(1, scaler.MinimalValue);
            Assert.AreEqual(0.99f, scaler.Scale, 0.001);
        }
        
        [TestCase(-0.1f)]
        [TestCase(1.1f)]
        public void Should_Fail_Create_When_Scale_Is_Not_Between_0_And_1(float scale)
        {
            Assert.Throws(typeof(ArgumentOutOfRangeException), delegate { new ValueScaler(10, 1, scale); });
        }

        [TestCase(10, 1, 0.05f, ExpectedResult = 1f)]
        [TestCase(10, 1, 0.9f, ExpectedResult = 9f)]
        [TestCase(10, 1, 0.5f, ExpectedResult = 5f)]
        [TestCase(20, 1, 0.9f, ExpectedResult = 18f)]
        [TestCase(20, 1, 0.5f, ExpectedResult = 10f)]
        public float Should_Apply_Multiplier(float startingValue, float minimalValue, float scale)
        {
            var scaler = new ValueScaler(startingValue, minimalValue,scale);
            return scaler.Apply();
        }
    }
}