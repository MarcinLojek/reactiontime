﻿using NUnit.Framework;

namespace ReactionTest.Tests
{
    public class ResultTest
    {
        [TestCase(0, 0, 0, float.MaxValue, 0, 0, 0)]
        public void Should_Create_New_Result(
            float reactionTime, int score, float sumaricalReactionTime,
            float shortestReactionTime, float meanReactionTime, float rate,
            float duration)
        {
            var result = Result.InitialResult();
            AssertResult(result, reactionTime, score, 
                sumaricalReactionTime, shortestReactionTime, meanReactionTime,
                rate, duration);
        }

        [TestCase(0.5f, 1, 0.5f, 0.5f, 0.5f / 1f, 1f / 1f, 1f)]
        public void Should_Update_Result(
            float reactionTime, int score, float sumaricalReactionTime,
            float shortestReactionTime, float meanReactionTime, float rate,
            float duration)
        {
            var result = Result.InitialResult();
            var partialResult = result.Update(0.5f, 1);
            AssertResult(partialResult, reactionTime, score, 
                sumaricalReactionTime, shortestReactionTime, meanReactionTime,
                rate, duration);
        }

        [TestCase(1f, 2, 1.5f, 0.5f, 1.5f / 2f, 2f / 2f, 2f)]
        public void Should_2xUpdate_Result(
            float reactionTime, int score, float sumaricalReactionTime,
            float shortestReactionTime, float meanReactionTime, float rate,
            float duration)
        {
            var result = Result.InitialResult();
            var partialResult = result.Update(0.5f, 1).Update(1, 2);
            AssertResult(partialResult, reactionTime, score, 
                sumaricalReactionTime, shortestReactionTime, meanReactionTime,
                rate, duration);
        }

        private static void AssertResult(Result result, float reactionTime, int score, float sumaricalReactionTime,
            float shortestReactionTime, float meanReactionTime, float rate, float duration)
        {
            Assert.AreEqual(reactionTime, result.ReactionTime);
            Assert.AreEqual(score, result.Score);
            Assert.AreEqual(sumaricalReactionTime, result.SumaricalReactionTime);
            Assert.AreEqual(meanReactionTime, result.AverageReactionTime);
            Assert.AreEqual(shortestReactionTime, result.ShortestReactioinTime);
            Assert.AreEqual(rate, result.Rate);
            Assert.AreEqual(duration, result.Duration);
        }
    }
}