﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;

namespace ReactionTest.Tests
{
    [TestFixture]
    public class AxisScalerTest
    {
        [Test]
        public void Should_Create_Axis_Scaler()
        {
            var scaler = new AxisScaler();
            Assert.NotNull(scaler);
        }

        [Test]
        public void Should_Scale_When_Array_Is_Empty()
        {
            var scaler = new AxisScaler();
            var scales = scaler.Scale(new List<float>().ToArray());
            Assert.NotNull(scales);
            Assert.AreEqual(0, scales.Length);
        }

        [TestCase(10)]
        public void Should_Scale_When_Array_Has_One_Value(params float[] data)
        {
            var scaler = new AxisScaler();
            var scales = scaler.Scale(data);
            Assert.NotNull(scales);
            Assert.AreEqual(1, scales.Length);
            Assert.AreEqual(0.5, scales[0]);
        }

        [TestCase(10, 15)]
        public void Should_Scale_When_Array_Has_Two_Values(params float[] data)
        {
            var scaler = new AxisScaler();
            var scales = scaler.Scale(data);
            Assert.NotNull(scales);
            Assert.AreEqual(2, scales.Length);
            Assert.AreEqual(0, scales[0]);
            Assert.AreEqual(1, scales[1]);
        }

        [TestCase(10, 15, 59, 32, 41)]
        public void Should_Scale_When_Array_Has_Many_Values(params float[] data)
        {
            var scaler = new AxisScaler();
            var scales = scaler.Scale(data);
            Assert.NotNull(scales);
            var min = data.Min();
            var max = data.Max();
            Assert.AreEqual(data.Length, scales.Length);
            for (var i = 0; i < data.Length; i++)
            {
                Assert.AreEqual(Mathf.InverseLerp(min, max, data[i]), scales[i]);
            }
        }
    }
}