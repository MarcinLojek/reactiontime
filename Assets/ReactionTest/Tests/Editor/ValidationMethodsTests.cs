﻿using System;
using NUnit.Framework;

namespace SteamGameDev.Tests
{
	[TestFixture]
	public class ValidationMethodsTests
	{
		public delegate void Del(int value,int min,int max);

		[TestCase(-20, 0.000001f)]
		[TestCase(50, 50)]
		[TestCase(101, 100)]
		public void ShouldFitToPecentageRange(float value, float expected)
		{
			var validated = ValidationMethods.FitToPercentageRange(value);

			Assert.AreEqual(validated, expected);
		}

		[TestCase(-20, 0.000001f)]
		[TestCase(50, 50)]
		public void ShouldFitToPositiveRange(float value, float expected)
		{
			var validated = ValidationMethods.FitToPercentageRange(value);

			Assert.AreEqual(validated, expected);
		}

		[TestCase(0.01f, 0.0001f, 0.0f, false)]
		[TestCase(0.01f, 0.0001f, 0.001f, false)]
		[TestCase(0.01f, 0.0001f, 0.01f, true)]
		[TestCase(-0.01f, 0.0001f, 0.01f, false)]
		public void ShouldCompareTwoValues(float firstValue, float secondValue, float epsilon, bool expected)
		{
			var areEqual = ValidationMethods.AreEqual(firstValue, secondValue, epsilon);

			Assert.AreEqual(areEqual, expected);
		}

		[TestCase(-10, 1, 5)]
		[TestCase(5, 1, 5)]
		[TestCase(10, 1, 5)]
		public void ShouldThrowExceptionIfGivenValueIsOutOfRange(int value, int min, int max)
		{
			Assert.That(() => ValidationMethods.IsValueInRange(value, min, max),
				Throws.Exception.TypeOf<ArgumentOutOfRangeException>());	
		}

		[TestCase(1, 1, 5)]
		[TestCase(2, 1, 5)]
		[TestCase(4, 1, 5)]
		public void ShouldNotThrowExceptionIfGivenValueIsInRange(int value, int min, int max)
		{
			ValidationMethods.IsValueInRange(value, min, max);
		}

		[Test]
		public void ShouldThrowExceptionIfArgumentIsNull()
		{
			Assert.That(() => ValidationMethods.IsNull(null),
				Throws.Exception.TypeOf<ArgumentNullException>());
		}

		[Test]
		public void ShouldNotThrowExceptionIfArgumentIsNotNull()
		{
			ValidationMethods.IsNull(new ValidationMethods());
		}

		[Test]
		public void ShouldThrowGivenException()
		{
			Assert.That(() => ValidationMethods.Validate(true, new ArgumentException("TEST")),
				Throws.Exception.TypeOf<ArgumentException>().And.Message.EqualTo("TEST"));	
		}

		[Test]
		public void ShouldNotThrowGivenException()
		{
			ValidationMethods.Validate(false, new ArgumentException("TEST"));
		}
	}
}