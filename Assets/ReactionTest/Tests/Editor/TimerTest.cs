﻿using NUnit.Framework;

namespace ReactionTest.Tests
{
    [TestFixture]
    public class TimerTest
    {
        private bool _flag;

        [SetUp]
        public void SetUp()
        {
            _flag = false;
        }

        [TestCase(1.5f)]
        [TestCase(1)]
        public void Should_Create_Timer(float startingTime)
        {
            var timer = new Timer(startingTime);
            Assert.AreEqual(startingTime, timer.StartingTime);
            Assert.AreEqual(0, timer.NormalizedElapsedTime);
            Assert.AreEqual(1, timer.NormalizedRemainingTime);
        }

        [TestCase(1, 1.5f, 1, 1 - 1)]
        [TestCase(1, -0.5f, 0.5f, 1 - 0.5f)]
        [TestCase(1.5f, 0.75f, 0.75f, 1.5f - 0.75f)]
        [TestCase(1, 0.5f, 0.5f, 1f - 0.5f)]
        public void Should_Update_Timer(float startingTime, float updateTime, float elapsedTime,
            float remainingTime)
        {
            var timer = new Timer(startingTime);
            timer.Update(updateTime);

            Assert.AreEqual(elapsedTime, timer.ElapsedTime);
            Assert.AreEqual(remainingTime, timer.RemainingTime);
            Assert.AreEqual(elapsedTime/(elapsedTime + remainingTime), timer.NormalizedElapsedTime);
            Assert.AreEqual(remainingTime/(elapsedTime + remainingTime), timer.NormalizedRemainingTime);
        }

        [Test]
        public void Should_Update_Timer_Two_Times_In_Row()
        {
            var timer = new Timer(1.5f);
            timer.Update(0.5f);
            timer.Update(0.75f);

            Assert.AreEqual(1.25f, timer.ElapsedTime);
            Assert.AreEqual(1.5f - 1.25f, timer.RemainingTime);
        }


        [Test]
        public void Should_Call_Event_When_Over()
        {
            var timer = new Timer(1);

            timer.OnOver += OnEvent;
            timer.Update(1.5f);

            Assert.True(_flag);
        }

        [Test]
        public void Should_Stop_Timer()
        {
            var timer = new Timer(1);
            timer.OnStop += OnEvent;
            timer.Update(0.5f);
            timer.Stop();
            timer.Update(0.5f);

            Assert.AreEqual(0.5f, timer.ElapsedTime);
            Assert.AreEqual(1f - 0.5f, timer.RemainingTime);
            Assert.True(_flag);
        }
        
        [Test]
        public void Should_Restart_Timer()
        {
            var timer = new Timer(1);
            timer.OnRestart += OnEvent;
            timer.Update(0.5f);
            timer.Restart();
            timer.Update(0.75f);

            Assert.AreEqual(0.75f, timer.ElapsedTime);
            Assert.AreEqual(1f - 0.75f, timer.RemainingTime);
            Assert.True(_flag);
        }
        
        [Test]
        public void Should_Restart_Timer_With_New_Starting_Time()
        {
            var timer = new Timer(1);
            timer.Restart(1.5f);

            Assert.AreEqual(1.5f, timer.StartingTime);
            Assert.AreEqual(0, timer.ElapsedTime);
            Assert.AreEqual(1.5f, timer.RemainingTime);
        }

        private void OnEvent()
        {
            _flag = true;
        }
    }
}